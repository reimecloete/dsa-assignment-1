import ballerina/test;
import ballerina/http;

http:Client stClient = check new ("http://localhost:9090");
 
    response = check clientEndpoint->get("/virtualLearningEnvironment/profile", {username:"Jackson_Marks", lastname:"Marks", firstname:"Jackson", preferred_formats:["audio", "video", "text"], subject:[{course:"Statistics"}]});
    io:println(response.toJsonString());

    response = check clientEndpoint->post("/virtualLearningEnvironment/profile", {username:"Sam_Chris", lastname:"Christine", firstname:"Sam", preferred_formats:["audio", "video", "text"], subject:[{course:"Maths"}]});
    io:println(response.toJsonString());

    response = check clientEndpoint -> put("/virtualLearningEnvironment/course", {video:"", audio:"", course:"Software Development", learning_obj:["topics"]});
    io:println(response.toJsonString());

   
