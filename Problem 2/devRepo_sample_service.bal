import ballerina/grpc;
import ballerina/log;

listener grpc:Listener ep = new (9090);

Fn[] fns = [];
fnDetail[] all_fns = [];

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "devRepo" on ep {

    remote function add_new_fn(functionInfo value) returns string|error {
        fns.push(value);
        return "Successfully added new function: " + value.fn.name;

        int currVersion = 0;
        foreach Fn fn in fns {
            if fn.name == value.fn.name {
                currVersion = fn.versionNum;
                if fn.versionNum == value.fn.versionNum {
                return error("That function already exists");
                }

            }
            
        }


    }
    remote function show_fn(string value) returns string|error {
        viewAll res = {list:all_fns};
        return viewAll;
    }
    remote function delete_fn(string value) returns string|error {
        boolean reorder = false;
        int i = 0;
        while i < fns.length(){
            if fns[i].name == value.funcName && reorder{
                fns[i].versionNum -=1;

            }
            if fns[i].name == value.funcName && fns[i].versionNum == value.versionNum && |reorder{
                fn err = fns.remove(i);
                reorder = true;
            }

        }
    }
    remote function add_fn(stream<string, grpc:Error?> clientStream) returns string|error {
       string multiFns;
       error? e = clientStream.forEach(function(Fn fns) {
           fns.push(multiFns);           
       });
       log:printInfo("Functions successfully added. ");
       
       grpc:Error? result;
       if (result is grpc:Error){
           log:printError("Error occured when adding functions. ");
       } else{
           log:printInfo("Adding functions... " );
       }

        
    }
    remote function show_all_fns(string value) returns stream<string, error?>|error {
        log:printInfo("View all versions of a function. ");
        string[] fns = [];
        int i = 0;
        foreach string fn in all_fns {
            all_fns[i] = fn;
            i += 1;
        }
        grpc:Error? result;
       if (result is grpc:Error){
           log:printError("Error occured when getting functions. ");
       } else{
           log:printInfo("Showing all functions... " );
       }
    }
    remote function show_all_criteria(stream<CriteriaMessage, grpc:Error?> clientStream) returns stream<string, error?>|error {
        check clientStream.forEach(function(CriteriaMessage CriteriasMsg){
            checkpanic caller->sendString(
                string '${CriteriaMsg.latest_versions}'
            );

        });
        check caller->complete();
}

