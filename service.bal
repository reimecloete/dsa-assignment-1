import ballerina/io;
import ballerina/grpc;

type learnersProfile record {
    string username;
    string lastname;
    string firstname;
    string[] preferred_formats;
    string[] past_subjects;
};
type learningMaterial record {
    float[] video;
    float[] audio;
    string course;
    string learning_obj;
};

};

service /virtualLearningEnvironment on new http:Listener(9090){
    resource function get profiles(string names) returns string|error{
       return learnersProfile + "Learner's profiles successfully retrieved "; 
    }
}
function course(string name) returns string {
    return "Courses of learner "+ learningMaterial;
}
   
