devRepoClient ep = check new ("http://localhost:9090");

public function main() {
    //add a new function
    log:printInfo("Add a new function or new version of a function");
    functionInfo = (fullName:"John", email:"john@gmail,com", language:"bal", functionality:"unary rpc");
  Fn fn = check ep->add_new_fn({});
  log:printInfo("new function added "+value.fn.name);

   //getting all the functions
   log:printInfo("show an existing function");
   var getResponse = devRepoBlockingEp->show_fn(all_fns);
   if (getResponse is error){
       log:printInfo("error showing existing functions.");
   }else{
       string result;
       log:printInfo("showing all functions "+viewAll);
       }

    //client streaming
    add_fnStreamingClient streamingClient = check ep->add_fn();
    string[] fn = [multiFns];
    foreach var Fn in fn {
        check streamingClient->sendString(Fn);
    }
    
    //server streaming
    stream<string, grpc:Error?> result check ep->show_all_fns(all_fns);
    check result.forEach(function(string str){
        log:printInfo(str);
    });

    //bi-directional streaming
    show_all_criteriaStreamingClient streamingClient = check ep->show_all_criteria();
    future<error?> f1 start readReponse(streamingClient);
    CriteriaMessage[] messages = [
        {latest_versions:" "}
    ],
    foreach CriteriaMessage msg in messages {
        check streamingClient->sendCriteriaMessage(msg);
    }
    check streamingClient->complet();
    check wait f1;
    function readResponse(show_all_criteriaStreamingClient streamingClient) returns error? {
        string? result = check streamingClient->receiveString();
        while !(result is()){
            log:printInfo(result);
            result = check streamingClient->receiveString(;)
        }
    }


   }


